import cv2
import csv
import pandas as pd 
from datetime import date

participants = pd.read_csv("ranklist.csv")

template_file_path = "certificate.jpg"

output_directory_path = "generated_certificates"

font_size = 1.5
font_color = (0,0,0)

ch= input("want to upload a new certificate (y/n):")
if ch=='y' or ch=='Y':
    certiName = input("Enter the student Name:")
    Sport = input("Enter the Acheived at:")
    text = [Sport,certiName]
    with open('ranklist.csv','a') as f:
        Csvwriter = csv.writer(f)
        Csvwriter.writerow(text)



today = date.today()

# dd/mm/YY
d1 = today.strftime("%d/%m/%Y")

for index, row in participants.iterrows():

    certiName = row["Name"].title()
    Sport = row["Sport"].title()
    img = cv2.imread(template_file_path)
    font = cv2.FONT_HERSHEY_SCRIPT_COMPLEX
    font2 = cv2.FONT_HERSHEY_COMPLEX_SMALL
    font3= cv2.FONT_HERSHEY_COMPLEX

    text = [Sport,certiName]
      
    for i in range(len(text)):
        if i== 0:
            textsize = cv2.getTextSize(text[i],font,0.5,10)
            cv2.putText(img, text[i], (440,310), font3, font_size, font_color, 2)
        else:
            textsize = cv2.getTextSize(text[i],1,0.5,2)
            cv2.putText(img, text[i], (470,455), font2, font_size, font_color, 2)
        
        textsize = cv2.getTextSize(d1,1,0.5,2)
        cv2.putText(img, d1, (250,520), font2, font_size, font_color,1)
        cv2.putText(img, "orchids", (690,530), font, font_size, font_color,1)


    certiPath = "{}/{}.png".format(output_directory_path,row["Name"])
    cv2.imwrite(certiPath, img)

    certiPath = output_directory_path+certiName+'.png'
    cv2.imwrite(certiPath, img)


c = input("Do you want the certificate (y/n): ")
if c=='y' or c =='Y' :
    flag = 0
    certiName = input("Enter the student Name:")
    for index, row in participants.iterrows():

        if certiName in row["Name"] :
            path =  output_directory_path +'/'+ certiName+'.png'
            img = cv2.imread(path)
            cv2.imshow('image',img)
            flag = 1

    if flag== 0 :
        print("No certificate found!!!")
cv2.waitKey(0)
cv2.destroyAllWindows()